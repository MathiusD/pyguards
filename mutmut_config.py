def init():
    pass


ignoredContents = [
    '@staticmethod'
]


def pre_mutation(context, **_):
    for ignored in ignoredContents:
        if ignored in context.current_source_line:
            context.skip = True
            return


def post_mutation(context, **_):
    pass
