from unittest import TestCase
from unittest.mock import MagicMock

from parameterized import parameterized
from pyguards.errors import raiseAssembledDictErrors, raiseAssembledListErrors, raiseAssembledErrors
from pyguards import errors as errorsModule


class TestErrors(TestCase):

    @parameterized.expand([
        ([], None),
        (None, AttributeError('errors must be defined')),
        ({}, AttributeError('errors must be a list (instead a dict)')),
        ("foo", AttributeError('errors must be a list (instead a str)')),
        ([AttributeError('example')], AttributeError([('example',)])),
        ([AttributeError('example'), AttributeError('notExample', 'ex')],
         AttributeError([('example',), ('notExample', 'ex')]))
    ])
    def testRaiseAssembledListErrors(self, errors: list, exceptionExpected: Exception):
        if exceptionExpected:
            with self.assertRaises(type(exceptionExpected)) as error:
                raiseAssembledListErrors(errors)
            self.assertEqual(error.exception.args, exceptionExpected.args)
        else:
            raiseAssembledListErrors(errors)

    @parameterized.expand([
        ({}, None),
        (None, AttributeError('errors must be defined')),
        ([], AttributeError('errors must be a dict (instead a list)')),
        ("foo", AttributeError('errors must be a dict (instead a str)')),
        ({'example': AttributeError('example')},
         AttributeError({'example': ('example',)})),
        ({'example': AttributeError('example'), 'composite': AttributeError('notExample', 'ex')},
         AttributeError({'example': ('example',), 'composite': ('notExample', 'ex')}))
    ])
    def testRaiseAssembledDictErrors(self, errors: dict, exceptionExpected: Exception):
        if exceptionExpected:
            with self.assertRaises(type(exceptionExpected)) as error:
                raiseAssembledDictErrors(errors)
            self.assertEqual(error.exception.args, exceptionExpected.args)
        else:
            raiseAssembledDictErrors(errors)

    @parameterized.expand([
        ({}, raiseAssembledDictErrors, ({}, )),
        (None, None, None, AttributeError('errors must be defined')),
        ([], raiseAssembledListErrors, ([], )),
        ("foo", None, None, NotImplementedError(
            'For raiseAssembledErrors, str isn\'t managed'))
    ])
    def testRaiseAssembledErrors(self, errors, functionCalled, expectedCall: tuple, exceptionExpected: Exception = None):
        if functionCalled is not None:
            if functionCalled.__name__ == "raiseAssembledDictErrors":
                errorsModule.raiseAssembledDictErrors = MagicMock(
                    name="def Mocked")
                mock = errorsModule.raiseAssembledDictErrors
            elif functionCalled.__name__ == "raiseAssembledListErrors":
                errorsModule.raiseAssembledListErrors = MagicMock(
                    name="def Mocked")
                mock = errorsModule.raiseAssembledListErrors
            else:
                raise NotImplementedError(
                    "Mock of %s isn't implemented in this test" % functionCalled.__name__)
        if exceptionExpected:
            with self.assertRaises(type(exceptionExpected)) as error:
                raiseAssembledErrors(errors)
            self.assertEqual(error.exception.args, exceptionExpected.args)
        else:
            raiseAssembledErrors(errors)
        if functionCalled is not None and expectedCall is not None:
            mock.assert_called_once_with(*expectedCall)
            if functionCalled.__name__ == "raiseAssembledDictErrors":
                errorsModule.raiseAssembledDictErrors = functionCalled
            elif functionCalled.__name__ == "raiseAssembledListErrors":
                errorsModule.raiseAssembledListErrors = functionCalled
