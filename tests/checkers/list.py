from unittest import TestCase

from parameterized import parameterized
from pyguards.checkers.list import checkListTypeContent


class TestList(TestCase):

    class Mock:
        pass

    @parameterized.expand([
        ([], int),
        ([], int, {'attribute': 'example'}),
        ([], int, {'other': 'example'}),
        ([12], int),
        ([12], int, {'attribute': 'example'}),
        ([12], int, {'other': 'example'}),
        (['ex'], str),
        (['ex'], str, {'attribute': 'example'}),
        (['ex'], str, {'other': 'example'}),
        (None, int, {}, AttributeError("value must be defined")),
        (None, int, {'attribute': 'example'}, AttributeError(
            "example must be defined")),
        (None, int, {'other': 'example'}, AttributeError(
            "value must be defined")),
        ("foo", str, {}, AttributeError("value must be a list (instead a str)")),
        ("foo", str, {'attribute': 'example'}, AttributeError(
            "example must be a list (instead a str)")),
        ("foo", str, {'other': 'example'}, AttributeError(
            "value must be a list (instead a str)")),
        (["foo"], int, {}, AttributeError("value[0] must be a int (instead a str)")),
        (["foo"], int, {'attribute': 'example'}, AttributeError(
            "example[0] must be a int (instead a str)")),
        (["foo"], int, {'other': 'example'}, AttributeError(
            "value[0] must be a int (instead a str)")),
        (["foo", 24], str, {}, AttributeError(
            "value[1] must be a str (instead a int)")),
        (["foo", 24], str, {'attribute': 'example'}, AttributeError(
            "example[1] must be a str (instead a int)")),
        (["foo", 24], str, {'other': 'example'}, AttributeError(
            "value[1] must be a str (instead a int)")),
        ([Mock()], object, {}),
        ([Mock()], object, {'attribute': 'example'}),
        ([Mock()], object, {'other': 'example'}),
        ([object()], Mock, {}, AttributeError(
            "value[0] must be a Mock (instead a object)")),
        ([object()], Mock, {'attribute': 'example'}, AttributeError(
            "example[0] must be a Mock (instead a object)")),
        ([object()], Mock, {'other': 'example'}, AttributeError(
            "value[0] must be a Mock (instead a object)")),
    ])
    def testCheckListTypeContent(self, value, typeExpected, kwargs: dict = {}, exceptionExpected=None):
        if exceptionExpected:
            with self.assertRaises(type(exceptionExpected)) as error:
                checkListTypeContent(value, typeExpected, **kwargs)
            self.assertEqual(error.exception.args, exceptionExpected.args)
        else:
            checkListTypeContent(value, typeExpected, **kwargs)
