from unittest import TestCase

from parameterized import parameterized
from pyguards.checkers.dict import checkDictTypeContent


class TestDict(TestCase):

    class Mock:
        pass

    @parameterized.expand([
        ({}, int),
        ({}, int, {'attribute': 'example'}),
        ({}, int, {'other': 'example'}),
        ({'ex': 12}, int),
        ({'ex': 12}, int, {'attribute': 'example'}),
        ({'ex': 12}, int, {'other': 'example'}),
        ({'ex': 'ex'}, str),
        ({'ex': 'ex'}, str, {'attribute': 'example'}),
        ({'ex': 'ex'}, str, {'other': 'example'}),
        (None, int, {}, AttributeError("value must be defined")),
        (None, int, {'attribute': 'example'}, AttributeError(
            "example must be defined")),
        (None, int, {'other': 'example'}, AttributeError(
            "value must be defined")),
        ("foo", str, {}, AttributeError("value must be a dict (instead a str)")),
        ("foo", str, {'attribute': 'example'}, AttributeError(
            "example must be a dict (instead a str)")),
        ("foo", str, {'other': 'example'}, AttributeError(
            "value must be a dict (instead a str)")),
        ({'ex': "foo"}, int, {}, AttributeError(
            "value['ex'] must be a int (instead a str)")),
        ({'ex': "foo"}, int, {'attribute': 'example'}, AttributeError(
            "example['ex'] must be a int (instead a str)")),
        ({'ex': "foo"}, int, {'other': 'example'}, AttributeError(
            "value['ex'] must be a int (instead a str)")),
        ({'ex': "foo", 'notEx': 24}, str, {}, AttributeError(
            "value['notEx'] must be a str (instead a int)")),
        ({'ex': "foo", 'notEx': 24}, str, {'attribute': 'example'}, AttributeError(
            "example['notEx'] must be a str (instead a int)")),
        ({'ex': "foo", 'notEx': 24}, str, {'other': 'example'}, AttributeError(
            "value['notEx'] must be a str (instead a int)")),
        ({'ex': Mock()}, object, {}),
        ({'ex': Mock()}, object, {'attribute': 'example'}),
        ({'ex': Mock()}, object, {'other': 'example'}),
        ({'ex': object()}, Mock, {}, AttributeError(
            "value['ex'] must be a Mock (instead a object)")),
        ({'ex': object()}, Mock, {'attribute': 'example'}, AttributeError(
            "example['ex'] must be a Mock (instead a object)")),
        ({'ex': object()}, Mock, {'other': 'example'}, AttributeError(
            "value['ex'] must be a Mock (instead a object)")),
    ])
    def testCheckDictTypeContent(self, value, typeExpected, kwargs: dict = {}, exceptionExpected=None):
        if exceptionExpected:
            with self.assertRaises(type(exceptionExpected)) as error:
                checkDictTypeContent(value, typeExpected, **kwargs)
            self.assertEqual(error.exception.args, exceptionExpected.args)
        else:
            checkDictTypeContent(value, typeExpected, **kwargs)
