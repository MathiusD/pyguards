from unittest import TestCase

from parameterized import parameterized
from pyguards.checkers.type import checkIsGoodType, checkTypeIfDefined


class TestType(TestCase):

    class Mock:
        pass

    @parameterized.expand([
        (12, int),
        (12, int, {'attribute': 'example'}),
        (12, int, {'other': 'example'}),
        ('ex', str),
        ('ex', str, {'attribute': 'example'}),
        ('ex', str, {'other': 'example'}),
        (None, int, {}, AttributeError("value must be defined")),
        (None, int, {'attribute': 'example'}, AttributeError(
            "example must be defined")),
        (None, int, {'other': 'example'}, AttributeError(
            "value must be defined")),
        ("foo", int, {}, AttributeError("value must be a int (instead a str)")),
        ("foo", int, {'attribute': 'example'}, AttributeError(
            "example must be a int (instead a str)")),
        ("foo", int, {'other': 'example'}, AttributeError(
            "value must be a int (instead a str)")),
        (24, str, {}, AttributeError("value must be a str (instead a int)")),
        (24, str, {'attribute': 'example'}, AttributeError(
            "example must be a str (instead a int)")),
        (24, str, {'other': 'example'}, AttributeError(
            "value must be a str (instead a int)")),
        (Mock(), object, {}),
        (Mock(), object, {'attribute': 'example'}),
        (Mock(), object, {'other': 'example'}),
        (object(), Mock, {}, AttributeError(
            "value must be a Mock (instead a object)")),
        (object(), Mock, {'attribute': 'example'}, AttributeError(
            "example must be a Mock (instead a object)")),
        (object(), Mock, {'other': 'example'}, AttributeError(
            "value must be a Mock (instead a object)")),
    ])
    def testCheckIsGoodType(self, value, typeExpected, kwargs: dict = {}, exceptionExpected=None):
        if exceptionExpected:
            with self.assertRaises(type(exceptionExpected)) as error:
                checkIsGoodType(value, typeExpected, **kwargs)
            self.assertEqual(error.exception.args, exceptionExpected.args)
        else:
            checkIsGoodType(value, typeExpected, **kwargs)

    @parameterized.expand([
        (12, int),
        (12, int, {'attribute': 'example'}),
        (12, int, {'other': 'example'}),
        ('ex', str),
        ('ex', str, {'attribute': 'example'}),
        ('ex', str, {'other': 'example'}),
        (None, int, {}),
        (None, int, {'attribute': 'example'}),
        (None, int, {'other': 'example'}),
        ("foo", int, {}, AttributeError("value must be a int (instead a str)")),
        ("foo", int, {'attribute': 'example'}, AttributeError(
            "example must be a int (instead a str)")),
        ("foo", int, {'other': 'example'}, AttributeError(
            "value must be a int (instead a str)")),
        (24, str, {}, AttributeError("value must be a str (instead a int)")),
        (24, str, {'attribute': 'example'}, AttributeError(
            "example must be a str (instead a int)")),
        (24, str, {'other': 'example'}, AttributeError(
            "value must be a str (instead a int)")),
        (Mock(), object, {}),
        (Mock(), object, {'attribute': 'example'}),
        (Mock(), object, {'other': 'example'}),
        (object(), Mock, {}, AttributeError(
            "value must be a Mock (instead a object)")),
        (object(), Mock, {'attribute': 'example'}, AttributeError(
            "example must be a Mock (instead a object)")),
        (object(), Mock, {'other': 'example'}, AttributeError(
            "value must be a Mock (instead a object)")),
    ])
    def testCheckTypeIsDefined(self, value, typeExpected, kwargs: dict = {}, exceptionExpected=None):
        if exceptionExpected:
            with self.assertRaises(type(exceptionExpected)) as error:
                checkTypeIfDefined(value, typeExpected, **kwargs)
            self.assertEqual(error.exception.args, exceptionExpected.args)
        else:
            checkTypeIfDefined(value, typeExpected, **kwargs)
