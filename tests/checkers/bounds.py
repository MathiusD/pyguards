from unittest import TestCase

from parameterized import parameterized
from pyguards.checkers.bounds import checkInBounds


class TestBounds(TestCase):

    @parameterized.expand([
        (12, 10, 20),
        (12, 10, 20, {'attribute': 'example'}),
        (12, 10, 20, {'other': 'example'}),
        (12, 14, 24, {}, AttributeError("value must be in following bounds (14-24)")),
        (12, 14, 24, {'attribute': 'example'},
         AttributeError("example must be in following bounds (14-24)")),
        (12, 14, 24, {'other': 'example'}, AttributeError(
            "value must be in following bounds (14-24)")),
        (1.0, 0.5, 1.5),
        (1.0, 1.25, 1.5, {}, AttributeError(
            "value must be in following bounds (1.25-1.5)")),
        (1.0, 1.25, 1.5, {'attribute': 'example'}, AttributeError(
            "example must be in following bounds (1.25-1.5)")),
        (1.0, 1.25, 1.5, {'other': 'example'}, AttributeError(
            "value must be in following bounds (1.25-1.5)")),
        (None, 1.25, 1.5, {}, AttributeError("value must be defined")),
        (None, 1.25, 1.5, {'attribute': 'example'}, AttributeError(
            "example must be defined")),
        (None, 1.25, 1.5, {'other': 'example'}, AttributeError(
            "value must be defined")),
        (2.0, None, 1.5, {}, AttributeError(
            "value must be in following bounds (<=1.5)")),
        (2.0, None, 1.5, {'attribute': 'example'}, AttributeError(
            "example must be in following bounds (<=1.5)")),
        (2.0, None, 1.5, {'other': 'example'}, AttributeError(
            "value must be in following bounds (<=1.5)")),
        (0.25, 0.5, None, {}, AttributeError(
            "value must be in following bounds (>=0.5)")),
        (0.25, 0.5, None, {'attribute': 'example'}, AttributeError(
            "example must be in following bounds (>=0.5)")),
        (0.25, 0.5, None, {'other': 'example'}, AttributeError(
            "value must be in following bounds (>=0.5)")),
        (None, None, None, {}, AttributeError("bounds must be defined")),
        (None, None, None, {'attribute': 'example'}, AttributeError(
            "bounds must be defined")),
        (None, None, None, {'other': 'example'}, AttributeError(
            "bounds must be defined")),
        (None, 0.5, 1.5, {}, AttributeError("value must be defined")),
        (None, 0.5, 1.5, {'attribute': 'example'}, AttributeError(
            "example must be defined")),
        (None, 0.5, 1.5, {'other': 'example'}, AttributeError(
            "value must be defined"))
    ])
    def testCheckInBounds(self, value, lowerBound, higherBound, kwargs: dict = {}, exceptionExpected=None):
        if exceptionExpected:
            with self.assertRaises(type(exceptionExpected)) as error:
                checkInBounds(value, lowerBound, higherBound, **kwargs)
            self.assertEqual(error.exception.args, exceptionExpected.args)
        else:
            checkInBounds(value, lowerBound, higherBound, **kwargs)
