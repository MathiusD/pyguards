from doctest import testmod

import pyguards
import pyguards.checkers
import pyguards.errors

# Launch DocTest
results = []

results.append(testmod(pyguards))
results.append(testmod(pyguards.checkers))
results.append(testmod(pyguards.checkers.bounds))
results.append(testmod(pyguards.checkers.defined))
results.append(testmod(pyguards.checkers.dict))
results.append(testmod(pyguards.checkers.list))
results.append(testmod(pyguards.checkers.rgb))
results.append(testmod(pyguards.checkers.type))
results.append(testmod(pyguards.errors))

for result in results:
    if result.failed > 0:
        exit(1)

print('DocTest success !')
