# PyGuards

[![Pipeline Status](https://gitlab.com/MathiusD/pyguards/badges/master/pipeline.svg)](https://gitlab.com/MathiusD/pyguards/-/pipelines)
[![Coverage Report](https://gitlab.com/MathiusD/pyguards/badges/master/coverage.svg)](https://mathiusd.gitlab.io/pyguards/reports/coverage/3.9)
[Mutation Report](https://mathiusd.gitlab.io/pyguards/reports/mutation/3.9)
[![PyPI](https://img.shields.io/pypi/v/pyguards)](https://pypi.org/project/pyguards)

Checks and Guards for Python
