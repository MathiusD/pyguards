ROOT_DIR:=$(shell dirname $(realpath $(firstword $(MAKEFILE_LIST))))

ifndef TEST_ENV_NAME
	TEST_ENV_NAME=local
endif

pip:
	@pip3 install -r requirements.txt

pipTesting: pip
	@pip3 install -r tests-requirements.txt

batchTests:
	@pip3 install tox
	@tox

localTests: pipTesting tests

tests: unittest doctest show_reports

unittest:
	@python3 -m xmlrunner -v -o=reports/$(TEST_ENV_NAME)

doctest:
	@python3 doctestRunner.py

coverage:
	@rm -f .coverage
	@coverage run -m unittest

coverage_report: coverage
	@coverage report --include=pyguards/*.py
	@coverage xml --include=pyguards/*.py -o reports/$(TEST_ENV_NAME)/coverageReport.xml
	@coverage html --include=pyguards/*.py -d reports/$(TEST_ENV_NAME)/coverage

mutmut: coverage
	@rm -f .mutmut-cache
	-@mutmut run --use-coverage

recompile:
	@python3 -m compileall -f pyguards/*

mutmut_report: mutmut recompile
	@mutmut junitxml >> reports/$(TEST_ENV_NAME)/mutmutReport.xml
	@mutmut html
	@rm -rf reports/$(TEST_ENV_NAME)/mutmut
	@mv html reports/$(TEST_ENV_NAME)/mutmut

show_reports: coverage_report mutmut_report
	@echo "Coverage report generated at file://$(ROOT_DIR)/reports/$(TEST_ENV_NAME)/coverage/index.html"
	@echo "Mutation report generated at file://$(ROOT_DIR)/reports/$(TEST_ENV_NAME)/mutmut/index.html"

clearTests:
	@rm -rf reports

pipBuild:
	@pip3 install --upgrade pip
	@pip3 install --upgrade build
	@pip3 install -r build-requirements.txt

build: clearBuild pipBuild
	@python3 -m build

clearBuild:
	@rm -rf dist/ *.egg-info

buildUploadTesting: build pipBuild
	@python3 -m twine upload --repository testpypi dist/*

buildUpload: build pipBuild
	@python3 -m twine upload dist/*